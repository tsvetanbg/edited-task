from pathlib import Path
from selenium import webdriver
from scrapy.http import Response
from scrapy.selector import Selector
import scrapy
import json


class QuotesSpider(scrapy.Spider):
    name = "q"
    start_urls = [
        "https://shop.mango.com/bg-en/women/skirts-midi/midi-satin-skirt_17042020.html?c=99",
        "https://shop.mango.com/bg-en/men/t-shirts-plain/100-linen-slim-fit-t-shirt_47095923.html?c=07"
    ]

    def parse(self, response: Response):
        driver = webdriver.Chrome()
        driver.get(response.url)
        
        s = Selector(text=driver.page_source)

        product = {
            'name': s.css('.product-name::text').get(),
            'price': s.css('.text-title-xl::text').get(),
            'colour': s.css('.colors-info-name::text').get(),
            'size': s.css('.gk2V5::text').getall()
        }
        
        product['price'] = product["price"].split(" ")[-1]

        print(f'\n \n \n {product["name"]}\n \n \n ')
        
        Path(f'{product["name"]}.json').write_text(json.dumps(product))

        driver.quit()
